let numb;
while (true) {
  const input  = +prompt("Введите номер!");
  numb = input;
  if (Number.isInteger(numb)) {

    break;
  }
}

if (numb < 5) {
  console.log("Sorry, no numbers");
} else {
  for (let i = 0; i <= numb; i++) {
    if (i % 5 == 0) {
      console.log(i);
    }
  }
}
